# ApiTestAutomation GitRepo

Dear Reader, for the Assessment,amongst 3 options from  https://any-api.com/ , https://apilist.fun/ , https://github.com/public-apis/public-apis 

i've selected GitHub Repository Creation,Read and Delete APIs to automate with below Apis.

https://docs.github.com/en/rest/repos/repos#create-a-repository-for-the-authenticated-user
https://docs.github.com/en/rest/repos/repos#get-a-repository
https://docs.github.com/en/rest/repos/repos#delete-a-repository
https://docs.github.com/en/rest/users/users#get-the-authenticated-user
https://docs.github.com/en/rest/repos/repos#list-repositories-for-the-authenticated-user

Basically , those apis are being used to  create ,delete  and list repositories in authenticated GitHub user's account.

I've prepared a project with REST Assured library using JAVA language.

## Getting started

First, who is going to use this automation project needs to have a GitHub account and  get a Personal Access Token on GitHub from below link

https://github.com/settings/tokens/new 

While selecting scope,all repository actions should be available for token.

After getting the token, "token" variable on the [[global.properties file]](src/test/java/resources/global.properties)  should be set with GitHub personal access.

## Adjust your test cases

I've used BDD framework using Cucumber Framework

In [[feature file]](src/test/java/Feature/RepositoryValidations.feature) , user should write down the test scenarios with using Gherkin syntax.

User can use below Gherkin Statements while setting the scenario.

1. `-User calls {string} request with {string}` -->This statement is being used to call one of the http request type listed on the [[APIResources]](src/test/java/resources/APIResources.java)]  with or without authorization.
      `-CreateRepoAPI`
      `-GetRepoAPI`
      `-DeleteRepoAPI`
      `-GetUserAPI`
      `-ListUserRepoAPI`


2. `-Verify api call got success with {int} status` -->This statement is being used to verify the response status code. 

3. `-Verify Response body's {string} variable's value is {string}` -->This statement is being used to check if  the response has desired variable with desired value.

4. `Repository payload creation with {string} {string} {string}` -->This statement is being used to build a payload for Create repository request.

I've already set some scenario on the feature file with Examples please refer to that.

## Running Tests

Test Cases can be run multiple ways

1. From the feature file 
2. From the TestRunner class
3. Running "mvm test" command on terminal (mvn test verify also can be used for test results reports)

## Used Methods

All test steps are defined with methods coming from [[StepDefinition]](src/test/java/stepDefinitions/StepDefinition.java) Class
 - All the methods are meant to work generically.

[[TestDataBuilder]](src/test/java/resources/TestDataBuilder.java) class is being called to Create a Payload with [[CreateRepositoryBody]](src/main/java/models/CreateRepositoryBody.java) Model (Using getter and setter method)

[[ReUsableMethods]](src/main/java/models/ReUsableMethods.java) class is being called to parse the responses from string to json.

[[Utils]](src/test/java/resources/Utils.java) class is being used for setting up the request with RequestSpecification method 

For all requests, PrintStream method is being used to log all details to logging.txt file

[[Utils]](src/test/java/resources/Utils.java) class is being used for getting the global values (Base URL , Token)

Also Hooks class has been created and is being used for creating pre-conditions for test cases.

## Reporting

Cucumber html reports is being generated to [[cucumber-html-reports]](target/cucumber-html-reports) path after verifying the tests 

## CI/CD integration

[Yaml file](.gitlab-ci.yml) is prepared for Gitlab CI integration. Tests will run everytime when anything being pushed to repository.

Github version is also set but while pushing repo, github detects the token and revokes it. So better not to use Github for this.
