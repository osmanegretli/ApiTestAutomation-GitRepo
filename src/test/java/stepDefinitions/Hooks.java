package stepDefinitions;

import io.cucumber.java.Before;
import org.apache.commons.lang3.RandomStringUtils;
import resources.TestDataBuilder;

import java.io.IOException;

import static stepDefinitions.StepDefinition.*;
import static stepDefinitions.StepDefinition.userInfoExtension;

public class Hooks {

    @Before("@CheckRepoNoAuth")
    public void beforeScenario() throws IOException {

        String generatedString = TestDataBuilder.repoNameGenerator(10);
        StepDefinition m = new StepDefinition();
        m.user_calls_an_http_request_with("GetUserAPI", "Auth");
        repoLinkExtension = ("" + userInfoExtension + "/Repo");
        //UserInfoExtension variable has been used here , since with every different user token link extension varies
        m.user_calls_an_http_request_with("ListUserRepoAPI", "Auth");
        if (stringResponse.contains("full_name:" + userInfoExtension + "/"+generatedString)) {
            m.user_calls_an_http_request_with("DeleteRepoAPI", "Auth");
        }
        m.repo_payload_creation_with(generatedString, "Public repo with no auth Check", "False");
        m.user_calls_an_http_request_with("CreateRepoAPI", "Auth");
    }
}

