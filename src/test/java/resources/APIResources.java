package resources;

public enum APIResources {

    CreateRepoAPI("user/repos"),
    GetRepoAPI("repos/"),
    DeleteRepoAPI("repos/"),
    GetUserAPI("user"),
    ListUserRepoAPI("users/");

    private String resource;

    APIResources(String resource) {

        this.resource = resource;
    }

    public String getResource() {

        return resource;
    }

}
