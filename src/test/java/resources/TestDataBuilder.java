package resources;

import models.CreateRepositoryBody;
import org.apache.commons.lang3.RandomStringUtils;


public class TestDataBuilder {

    public CreateRepositoryBody createRepoPayload(String Name,String Description , Boolean isPrivate) {

        CreateRepositoryBody repoBody = new CreateRepositoryBody();
        repoBody.setDescription(Description);
        repoBody.setPrivate(isPrivate);
        repoBody.setName(Name);
        return repoBody;
    }

    public static String repoNameGenerator (int characterCount) {

        String generatedString = RandomStringUtils.randomAlphabetic(characterCount);
        return generatedString;
    }
}
